PongThreeJS
===========

#### What

PongThreeJS is an online multiplayer pong game. This game is part of FI-WARE (http://www.fi-ware.eu/). Final purpose is to make a realXtend web client. This is a use case to support that goal.

* **Features**
  - ThreeJS 3D javascript library http://threejs.org/
  - Ammo.js / Bullet physics (http://bulletphysics.org/ https://github.com/kripken/ammo.js/)
  - 2 to 100 players multiplayer capability
  - Dynamically increasing and decreasing game area.

#### Why

Pong is a great use case, because it's minimalistic and still it can provide a full test case. Multi-user environment allows us to do e.g. network testing and checking with different user counts. This was Jarkko's idea so credits to him.

#### How

First we created the game without additional frameworks like Bullet physics. Finally we will include an entity system. It makes everything straightforward, easier and better, as we need less code and the game is overall better.

#### Conclusion

?
